# NotebookLab

[![build status](https://gitlab.com/gitlab-org/notebooklab/badges/master/build.svg)](https://gitlab.com/gitlab-org/notebooklab/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/notebooklab/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/notebooklab/commits/master)

NotebookLab is a Vue component that makes it easy to render IPython notebooks. It's as simple as
passing in the returned IPython JSON and the component renders.

The component is not opinionated on how the notebook is fetched. Your applications should handle
this and then just pass in the parsed JSON file. See the [example](#example) below.

Supports:

- Markdown
- Code highlighting with [Prism.JS](http://prismjs.com/)
  - CSS is compatible with [pygments](http://pygments.org/)
- [Katex](https://github.com/Khan/KaTeX) library

## Usage

Install the plugin by adding the JS file and then install the plugin

```javascript
Vue.use(NotebookLab);
```

Then you can instantiate the component by just doing `<notebook-lab />`

```html
<notebook-lab :notebook="notebookJson" />
```

The only required props is `notebook` and this is the parsed JSON from the notebook you want to
display.

For GitLab, we allow different syntax highlighting colors and this plugin allows the colors to be
changed with the prop `code-css-class`.

```html
<notebook-lab :notebook="notebookJson" :code-css-class="code white" />
```

## Example

A simple example that shows how the notebook can be fetched and parsed with `fetch`

```javascript
import NotebookLab from 'notebooklab';

new Vue({
  el: '#js-test',
  template: `
    <notebook-lab :notebook="json" />
  `,
  data() {
    return {
      json: {},
    };
  },
  mounted() {
    fetch('test.ipynb')
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.json = data;
      })
  },
});
```

import Vue from 'vue';
import PromptComponent from '../prompt.vue';

const Component = Vue.extend(PromptComponent);

describe('Prompt component', () => {
  let vm;

  describe('input', () => {
    beforeEach((done) => {
      vm = new Component({
        propsData: {
          type: 'In',
          count: 1,
        },
      });
      vm.$mount();

      setTimeout(() => {
        done();
      });
    });

    test('renders in label', () => {
      expect(vm.$el.textContent.trim()).toContain('In');
    });

    test('renders count', () => {
      expect(vm.$el.textContent.trim()).toContain('1');
    });
  });

  describe('output', () => {
    beforeEach((done) => {
      vm = new Component({
        propsData: {
          type: 'Out',
          count: 1,
        },
      });
      vm.$mount();

      setTimeout(() => {
        done();
      });
    });

    test('renders in label', () => {
      expect(vm.$el.textContent.trim()).toContain('Out');
    });

    test('renders count', () => {
      expect(vm.$el.textContent.trim()).toContain('1');
    });
  });
});

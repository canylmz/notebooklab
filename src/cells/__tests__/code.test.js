import Vue from 'vue';
import json from '../../../fixtures/file.json';
import CodeComponent from '../code.vue';

const Component = Vue.extend(CodeComponent);

describe('Code component', () => {
  let vm;

  describe('without output', () => {
    beforeEach((done) => {
      vm = new Component({
        propsData: {
          cell: json.cells[0],
        },
      });
      vm.$mount();

      setTimeout(() => {
        done();
      });
    });

    test('does not render output prompt', () => {
      expect(vm.$el.querySelectorAll('.prompt').length).toBe(1);
    });
  });

  describe('with output', () => {
    beforeEach((done) => {
      vm = new Component({
        propsData: {
          cell: json.cells[2],
        },
      });
      vm.$mount();

      setTimeout(() => {
        done();
      });
    });

    test('does not render output prompt', () => {
      expect(vm.$el.querySelectorAll('.prompt').length).toBe(2);
    });

    test('renders output cell', () => {
      expect(vm.$el.querySelector('.output')).toBeDefined();
    });
  });
});

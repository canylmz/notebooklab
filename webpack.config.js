const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: {
    notebooklab: './index.js',
    'notebooklab.min': './index.js',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].js',
    libraryTarget: 'umd',
    library: 'NotebookLab',
    umdNamedDefine: true,
  },
  module: {
    loaders: [{
      test: /\.vue$/,
      use: ['vue-loader'],
      exclude: /node_modules/,
    }, {
      test: /\.js$/,
      use: ['babel-loader'],
      exclude: /node_modules/,
    }],
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      include: /\.min\.js$/,
      minimize: true,
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
  ],
};
